import { FIELD_TO_CLASS_MAP } from '../util/fieldClassMap';
import Router from '../routes/routing.handler';
import StudentService from '../services/student.service';
import Toast from './toast';

const studentTemplate = `
<div id="app-add-student-page" class="app-add-student-page">
  <div class="navbar bg-dark">
    <a class="navbar-brand text-light app-home-button">Student MS</a>
  </div>
  <div id="app-student-form-container" class="app-student-form-container">
    <form class="app-student-form">
      <div class="form-group">
        <label for="studentId">Id</label>
        <input type="text" class="form-control" id="studentId" placeholder="Student ID">
      </div>
      <div class="form-group">
        <label for="firstName">First Name</label>
        <input type="text" class="form-control" id="firstName" placeholder="First Name">
      </div>
      <div class="form-group">
        <label for="lastName">Last Name</label>
        <input type="text" class="form-control" id="lastName" placeholder="Last Name">
      </div>
      <div class="form-group">
        <label for="class">Class</label>
        <input type="text" class="form-control" id="class" placeholder="First Name">
      </div>
      <div class="form-group">
        <label for="city">City</label>
        <input type="text" class="form-control" id="city" placeholder="First Name">
      </div>
      <div class="form-group">
        <label for="state">State</label>
        <input type="text" class="form-control" id="state" placeholder="First Name">
      </div>
      <div class="form-group">
        <label for="country">Country</label>
        <input type="text" class="form-control" id="country" placeholder="First Name">
      </div>
      <button type="submit" class="btn btn-primary app-save-student">Submit</button>
      <button class="btn btn-success app-back-from-student">Back</button>
    </form>
  </div>
</div>
`;

export class StudentView {

  goHome = () => {
    Router.goTo('/');
  }

  notify = (message, type) => {
    let toast = Toast.getToast(message, type);
    let studentForm = document.querySelector(FIELD_TO_CLASS_MAP.studentForm);
    studentForm.insertAdjacentHTML('afterend', toast);
  }

  save = (event) => {
    event.preventDefault();

    let studentForm = document.querySelector(FIELD_TO_CLASS_MAP.studentForm);
    let formData = {};

    [...studentForm.elements].map(e => {
      formData[e.id] = e.value;
    });

    // send this to server
    StudentService.addStudent(formData);

    this.notify("Student Added Successfully", "success")
  }

  handleToastDismiss = (event) => {
    let classes = event.target.className.split(' ');

    if (classes.includes(FIELD_TO_CLASS_MAP.toastCloseButton.split('.')[1])) {
      let studentFormContainer = document.querySelector(FIELD_TO_CLASS_MAP.studentFormContainer);
      let toast = document.getElementById(FIELD_TO_CLASS_MAP.appToastId);
      studentFormContainer.removeChild(toast);
    }
  }

  // method to display the page
  render = () => {
    let root = document.querySelector(FIELD_TO_CLASS_MAP.root);
    root.innerHTML = studentTemplate;

    // event handllers
    let appHomeButton = document.querySelector(FIELD_TO_CLASS_MAP.appHomeButton);
    appHomeButton.addEventListener("click", this.goHome);

    let studentForm = document.querySelector(FIELD_TO_CLASS_MAP.studentForm);
    studentForm.addEventListener("submit", this.save);

    let goBackToHome = document.querySelector(FIELD_TO_CLASS_MAP.goBackFromStudent);
    goBackToHome.addEventListener("click", this.goHome);

    let addStudentPage = document.querySelector(FIELD_TO_CLASS_MAP.addStudentPage);
    addStudentPage.addEventListener("click", this.handleToastDismiss);
  }
}