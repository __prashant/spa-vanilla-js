import { FIELD_TO_CLASS_MAP } from '../util/fieldClassMap';
import Router from '../routes/routing.handler';
import StudentService from '../services/student.service';
import { Toast } from './toast';

const homeTemplate = `
<div>
  <div class="navbar bg-dark">
    <a class="navbar-brand text-light app-home-button">Student MS</a>
  </div>

  <div class="home_container">
    <div class="d-flex justify-content-end">
      <button class="btn btn-success app-add-student-btn">Add New Student</button>
    </div>

    <div class="home_table--container">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th>Student Id</td>
            <th>Name</td>
            <th>Class</td>
            <th>City</td>
            <th>State</td>
            <th>Country</td>
          </tr>
        </thead>
        <tbody class="app-student-details">
        </tbody>
      </table>
    </div>
  </div>
</div>
`;

export class HomeView {

  // method to append student details
  addStudentDetail = (students) => {
    let tableBody = document.querySelector(FIELD_TO_CLASS_MAP.studentDetails);

    students.map(student => {

      let studentDetailTemplate = `
      <tr>
        <td>${student.studentId}</td>
        <td>${`${student.firstName} ${student.lastName}`}</td>
        <td>${student.class}</td>
        <td>${student.city}</td>
        <td>${student.state}</td>
        <td>${student.country}</td>
      </tr>`;

      tableBody.insertAdjacentHTML("beforeend", studentDetailTemplate);
    });
  }

  addStudent = () => Router.goTo('/manageStudent');

  goHome = () => Router.goTo('/');

  // method to display the page
  render = () => {
    let root = document.querySelector(FIELD_TO_CLASS_MAP.root);
    // i know we should not use innerHTML, will replace it with a better solution
    root.innerHTML = homeTemplate;

    // add event handlers
    let addStudentBtn = document.querySelector(FIELD_TO_CLASS_MAP.addStudentBtn);
    addStudentBtn.addEventListener("click", this.addStudent);

    let appHomeButton = document.querySelector(FIELD_TO_CLASS_MAP.appHomeButton);
    appHomeButton.addEventListener("click", this.goHome);

    // render student details
    this.addStudentDetail(StudentService.getStudents());
  }

}