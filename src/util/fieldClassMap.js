export const FIELD_TO_CLASS_MAP = {
  root: '.app-root',
  appHomeButton: '.app-home-button',
  addStudentBtn: '.app-add-student-btn',
  studentDetails: '.app-student-details',
  studentForm: '.app-student-form',
  saveStudent: '.app-save-student',
  goBackFromStudent: '.app-back-from-student',
  studentFormContainer: '.app-student-form-container',
  appToastClass: '.app-toast',
  appToastId: 'app-toast',
  addStudentPage: '.app-add-student-page',
  addStudentPageId: 'app-add-student-page',
  toastCloseButton: '.app-toast-close-button'
}
